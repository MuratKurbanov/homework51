import React from "react";

const Card = (props) => {
    return (
        <div className="person">
            <h1>{props.title}</h1>
            <p>Year: {props.year}</p>
            <img src={props.img} alt=""/>
        </div>
    )
};

export default Card;