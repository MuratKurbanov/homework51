import React, { Component } from 'react';
import Card from "./Card/Card.js";
import "./Card/style.css";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Card title="Legend" year="2015" img="https://a.d-cd.net/9bb0c7as-480.jpg"/>
        <Card title="Avatar" year="2009" img="https://vokrug.tv/pic/news/5/b/e/8/5be8f1d1c4881777905e55ef78e9d5a3.jpg"/>
        <Card title="Deadpool" year="2018" img="http://flicksandthecity.com//wp-content/uploads/Deadpool-600x600.jpg"></Card>
      </div>
    );
  }
}

export default App;
